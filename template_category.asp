<!-- #include virtual="/WS_CORE/modules_inc.asp"-->
<!-- #include virtual="/ws_client_themes/inc/header.asp"-->
					<%=website_edit()%>
<body class="template_category">
	<!-- #include virtual="/ws_client_themes/inc/masthead.asp"-->

	<div id="mainvisual" class="section full subpage">
		<div class="slide" style="background-image:url(<%=module_category_image(1)%>);">
			<!-- <%=module_category_image(2)%> -->
			<div class="wrapper">
				<div class="content">
						<%
							t_category_banner_title = ""
							if len(page_page_category_variable_name) > 0 then
								Set RS = Server.CreateObject ("ADODB.Recordset")
								strSQL = "select variable_value from ws_page_variables where page_id = " & current_page_id & " and variable_name = '" & page_page_category_variable_name & "' and status >= 1000 order by id desc"
								rs.open strSQL, conn
							if not rs.eof and not rs.bof then
								    t_category_banner_title = rs.fields("variable_value")
							end if
								rs.close
							end if
							set rs = nothing
						 %>
						<%=t_category_banner_title %>
				</div>
			</div>
		</div>
	</div><!-- end #mainvisual -->

	<div class="section full contentarea">
		<div class="wrapper">
			<div class="section twothird maincontent left">
					<h1><%=current_page_title%></h1>
					<%=module_text(2)%>

 <%
                    'this is for white papers
					Set RS = Server.CreateObject ("ADODB.Recordset")
					strSQL = "select tgtlp.page_id, p.title, p.path from WS_Tag_Group_Tag_Library_Page as tgtlp join WS_Tag_Group_Tag_Library as tgtl on tgtl.tag_library_id = tgtlp.tag_group_tag_library_id"
					strSQL = strSQL & " join ws_tag_group as tg on tg.id = tgtl.tag_group_id  join ws_tag_library as tl on tl.id = tgtl.tag_library_id"
					strSQL = strSQL & " join WS_Page as p on p.ID = tgtlp.page_id where p.category_id = "& current_page_category_id &" and tl.id = 1 and p.Status >=1000 order by p.ID desc"
					rs.open strSQL, conn
					if not rs.eof and not rs.bof then
						Set RST = Server.CreateObject ("ADODB.Recordset")
						do while not rs.eof
						    prefix = ""
							t_page_id = clng(0)
							t_page_title = ""
							t_page_path = ""
							t_page_blurb = ""
							t_page_id = rs.fields("page_id")
							t_page_path = rs.fields("path")
							t_page_title = rs.fields("title")

							if len(t_page_title) > 0 then
								return_text = ""

								if len(page_page_title_variable_name) > 0 and clng(t_page_id) <> clng(0) then
									strSQL = "select variable_value from ws_page_variables where page_id = " & t_page_id & " and variable_name = '" & page_page_title_variable_name & "' and status >= 1000 order by id desc"
									'response.Write strSQL & "<br />"
									rst.open strSQL, conn
									if not rst.eof and not rst.bof then
										    t_page_title = rst.fields("variable_value")
									end if
									rst.close
								end if
								return_text = return_text & "<h3><a href=""" & basepath & t_page_path & """>" & prefix & t_page_title & "</a></h3>" & vbcrlf

								if len(page_page_blurb_variable_name) > 0 and clng(t_page_id) <> clng(0) then
									strSQL = "select variable_value from ws_page_variables where page_id = " & t_page_id & " and variable_name = '" & page_page_blurb_variable_name & "' and status >= 1000 order by id desc"
									rst.open strSQL, conn
									if not rst.eof and not rst.bof then
										t_page_blurb = rst.fields("variable_value")
									end if
									rst.close
								end if
								return_text = return_text & "<p>" & t_page_blurb & vbcrlf & "</p>" & vbcrlf

								response.write return_text
							end if

							rs.moveNext
						loop
						Set RST = Nothing
					end if
					rs.close
					Set RS = Nothing
%>
				<div id="forms"><!-- form -->
					<%=module_form(1)%>
				</div><!-- end of form -->

				<div class="bottomcta">
					<div class="widget cta item1"><%=module_text(27)%></div>
					<div class="widget cta item2"><%=module_text(28)%></div>
				</div>

			</div> <!-- end twothird maincontent -->
			<div class="section onethird sidebar right">

				<div class="widget cta item1">
                    <%=module_text(25)%>
				</div>
				<div class="widget cta item2">
                    <%=module_text(26)%>
				</div>
				<div class="widget catmenu">
					<%=module_menu_category(2) %>
				</div>

			</div> <!-- end onethird sidebar -->
		</div> <!-- end wrapper -->
	</div> <!-- end section about -->

	<!-- #include virtual="/ws_client_themes/inc/footer.asp"-->

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<!-- #include virtual="/ws_client_themes/inc/scripts.asp"-->

</body>
</html>