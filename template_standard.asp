<!-- #include virtual="/WS_CORE/modules_inc.asp"-->
<!-- #include virtual="/ws_client_themes/inc/header.asp"-->

				<%=website_edit()%>

<body class="template_standard">
	<!-- #include virtual="/ws_client_themes/inc/masthead.asp"-->

	<div class="section full contentarea">
		<div class="wrapper">
			<div class="section twothird maincontent left">
				<h1><%=current_page_title%></h1>

				<%=module_text(2)%>
				<%if current_page_path = job_search_result_path then%>
					<%=module_job_search_result()%>
				<%end if%>

				<div id="forms"><!-- form -->
					<%=module_form(1)%>
				</div><!-- end of form -->

				<% if lcase(current_page_path)= lcase("Site-Map") then%>
					<%=module_site_map(1)%>
				<%end if%>

				<%if lcase(current_page_path) = lcase(search_results_page) then
					%><%=module_search_result()%>
				<%  end if%>

				<% if (lcase(current_page_path)= lcase(Jobs_By_Type_Page) or lcase(current_page_path)= lcase(Jobs_By_State_Page) or lcase(current_page_path)= lcase(Open_Jobs)) then%>
					<%=module_jobs(1)%>
				<%end if%>
				<div class="bottomcta">
				<%temp_text = module_text(27)
				if len(module_text(27)) > 0 then %>
					<div class="widget cta item1"><%=temp_text%></div>
				<%end if
				temp_text = module_text(28)
				if len(module_text(28)) > 0 then %>
					<div class="widget cta item2"><%=temp_text%></div>
				<%end if%>
				</div>
			</div> <!-- end section twothird maincontent left -->
			<div class="section onethird sidebar right">

				<div class="widget cta contact">
				<%=module_text(24)%>
				</div>
				<div class="widget cta item2">
					<%=module_text(23)%>
				</div>

				<div class="widget catmenu">
					<%=module_menu_category(2) %>
				</div>

			</div> <!-- end onethird sidebar -->
		</div> <!-- end wrapper -->
	</div> <!-- end section full contentarea -->

	<!-- #include virtual="/ws_client_themes/inc/footer.asp"-->

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<!-- #include virtual="/ws_client_themes/inc/scripts.asp"-->

</body>
</html>

<%=module_tracker()%>
<!-- #include virtual="WS_CORE/inc_cleanup.asp"-->
