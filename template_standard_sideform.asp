<!-- #include virtual="/WS_CORE/modules_inc.asp"-->
<!-- #include virtual="/ws_client_themes/inc/header.asp"-->

				<%=website_edit()%>
<body class="template_sideform">
	<!-- #include virtual="/ws_client_themes/inc/masthead.asp"-->
	<div class="section full contentarea">

		<div class="wrapper">
			<div class="section half maincontent left">
				<h1><%=current_page_title%></h1>

				<%=module_text(2)%>
				
				<div class="bottomcta">
					<div class="widget cta item1"><%=module_text(27)%></div>
					<div class="widget cta item2"><%=module_text(28)%></div>
				</div>
			</div> <!-- end section half maincontent left -->
			<div class="section half sidebar right">
                    		<%
							t_form_title = ""
							if len(page_page_form_variable_name) > 0 then
								Set RS = Server.CreateObject ("ADODB.Recordset")
								strSQL = "select variable_value from ws_page_variables where page_id = " & current_page_id & " and variable_name = '" & page_page_form_variable_name & "' and status >= 1000 order by id desc"
								rs.open strSQL, conn
							    if not rs.eof and not rs.bof then
								        t_form_title = rs.fields("variable_value")
							    end if
								rs.close
							end if
							set rs = nothing
						 %>
					<span class="form_title"><%=t_form_title %></span>
					<%=module_form(1)%>

			</div> <!-- end section half sidebar right -->
		</div> <!-- end wrapper -->
	</div> <!-- end section full contentarea -->

	<!-- #include virtual="/ws_client_themes/inc/footer.asp"-->

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
	<!-- #include virtual="/ws_client_themes/inc/scripts.asp"-->

</body>
</html>

<%=module_tracker()%>
<!-- #include virtual="WS_CORE/inc_cleanup.asp"-->
